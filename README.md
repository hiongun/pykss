**A Pure Python Korean Sentence Separator.**

순수 파이선으로 만든 한글 문장 분리기 입니다.

**How to use:**



import pykss

text = "multi line bulk text.. \t 한글 텍스트. 마침표 같은 힌트를 찾아서 나눈다. \n 이런 문장 도 되나? "

sentences = pykss.split_sentences(text)

